import React, { Component } from "react";
import "./App.css";

import Status from "./Status";
import Board from "./Board";
import History from "./History";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // Determines who is the winner of the game, null means nobody
      /*winner: {
        symbol: null,
        crossLine: null,
        order: null
      },*/
      winner: this.calcWinner(Array(9).fill(null)),

      history: [
        {
          key: 0,
          // field[i] = '0'|'X' if square clicked, null means unclicked
          field: Array(9).fill(null),
          // next move. Which player will move next?
          next: "X"
        }
      ]
    };
    this.handleMove = this.handleMove.bind(this);
    this.handleJump = this.handleJump.bind(this);
    this.calcWinner = this.calcWinner.bind(this);
  }

  // Helper function. PURE
  // parameters: field - Array[9] of "X", "O", or null
  calcWinner(field) {
    // combinations in Array(9) that win
    const combinations = [
      {
        // 1st row
        combination: [0, 1, 2],
        crossLine: "row",
        order: 1
      },
      {
        // 2nd row
        combination: [3, 4, 5],
        crossLine: "row",
        order: 2
      },
      {
        // 3rd row
        combination: [6, 7, 8],
        crossLine: "row",
        order: 3
      },
      {
        // 1st column
        combination: [0, 3, 6],
        crossLine: "column",
        order: 1
      },
      {
        // 2nd column
        combination: [1, 4, 7],
        crossLine: "column",
        order: 2
      },
      {
        // 3rd column
        combination: [2, 5, 8],
        crossLine: "column",
        order: 3
      },
      {
        // top-left to bottom-rignt diagonal
        combination: [0, 4, 8],
        crossLine: "diagonal",
        order: 1
      },
      {
        // bottom-left to top-right diagonal
        combination: [2, 4, 6],
        crossLine: "diagonal",
        order: 2
      }
    ];
    //default
    let winner = {
      symbol: null,
      crossLine: null,
      order: null
    };

    for (let i = 0; i < combinations.length; i++) {
      // on each step extract line to variables a,b and c
      const [a, b, c] = combinations[i].combination;
      if (
        // check if field[a] === field[b] === field[c]
        field[a] &&
        field[a] === field[b] &&
        field[a] === field[c]
      ) {
        // "X" or "O"
        winner.symbol = field[a];
        winner.crossLine = combinations[i].crossLine;
        winner.order = combinations[i].order;
      }
    }
    return winner;
  }

  // Handler for click on Board event
  handleMove(squareNum) {
    const lastIndex = this.state.history.length - 1;
    const lastField = this.state.history[lastIndex].field;
    const lastNext = this.state.history[lastIndex].next;

    // Check if square is already filled with "X" or "O"
    const clickAllowed = !lastField[squareNum];
    // Check if nobody won
    const noWinner = !this.state.winner.symbol;

    if (clickAllowed && noWinner) {
      // Add clicked item to Array
      const newField = lastField.map((item, index) => {
        return index === squareNum ? lastNext : item;
      });

      //Calculate if we have winner or not
      const winner = this.calcWinner(newField);

      const history = this.state.history.concat({
        key: Date.now(),
        field: newField,
        // Toggle next
        next: lastNext === "X" ? "O" : "X"
      });

      // Save total state with new actual data
      this.setState({ history, winner });
    }
  }

  // Handler for history jump event
  handleJump(move) {
    const newHistory = this.state.history.slice(0, move + 1);
    this.setState({
      history: newHistory,
      //default
      winner: this.calcWinner(Array(9).fill(null))
    });
  }

  render() {
    // Render move number and who is the next player ("X" or "O")
    const moveNumber = this.state.history.length;
    let status =
      "Move #" + moveNumber + ": " + this.state.history[moveNumber - 1].next;

    // Rewrite status line if game ended (we have winner)
    if (this.state.winner.symbol) {
      status = this.state.winner.symbol + " won the game!";
    }

    const lastField = this.state.history[this.state.history.length - 1].field;

    return (
      <div className="App">
        <Status status={status} />
        <Board
          onClick={this.handleMove}
          field={lastField}
          winner={this.state.winner}
        />
        <History onClick={this.handleJump} history={this.state.history} />
      </div>
    );
  }
}

export default App;
