import React from "react";
import "./Square.css";

const Square = props => {
  return (
    <button className="Square" onClick={props.onClick}>
      <div className="Square__symbol">{props.value}</div>
    </button>
  );
};

export default Square;
