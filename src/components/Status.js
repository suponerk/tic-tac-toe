import React from "react";
import "./Status.css";

const Status = props => {
  return <div className="Status">{props.status}</div>;
};

export default Status;
