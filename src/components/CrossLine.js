import React from "react";
import "./CrossLine.css";

const CrossLine = props => {
  let width = props.line === "column" ? "0.3em" : "100%";
  let height = props.line === "column" ? "100%" : "0.3em";

  // Let`s calculate shift
  const segment = 100 / 6;
  const delta = segment * 2 * (props.order - 1);
  const shift = "calc(" + (segment + delta) + "% - 0.15em";

  let top = "0";
  let left = "0";

  if (props.line === "row") top = shift;
  if (props.line === "column") left = shift;

  // Let`s calculate rotation in case of diagonals
  let transform = "none";
  if (props.line === "diagonal") {
    const angle = 45 + (props.order - 1) * 90;
    //console.log("angle:", angle);
    transform = "skewY(" + angle + "deg)";
    top = "calc(50% - 0.15em)";
    height = "0.4em";
  }

  let style = { width, height, top, left, transform };

  return <div className="CrossLine" style={style}></div>;
};

export default CrossLine;
