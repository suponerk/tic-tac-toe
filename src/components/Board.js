import React from "react";
import "./Board.css";

import Square from "./Square";
import CrossLine from "./CrossLine"

const Board = props => {
  // Helper to render miltiple squares
  const renderSquare = squareNum => {
    return (
      <Square
        value={props.field[squareNum]}
        onClick={() => props.onClick(squareNum)}
        angle={props.angle}
      />
    );
  };
  const renderCrossLine = hasWinner => {
    return hasWinner ? <CrossLine line={props.winner.crossLine} order={props.winner.order} /> : "";
  }
  
  return (
    <div className="Board">
      {renderSquare(0)}
      {renderSquare(1)}
      {renderSquare(2)}
      {renderSquare(3)}
      {renderSquare(4)}
      {renderSquare(5)}
      {renderSquare(6)}
      {renderSquare(7)}
      {renderSquare(8)}
      {renderCrossLine(props.winner.symbol)}
    </div>
  );
};

export default Board;
