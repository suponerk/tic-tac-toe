import React from "react";
import "./History.css";

const History = props => {
  const moves = props.history.slice(1).map((item, index) => {
    const label = index ? "Jump to move #" + (index + 1) : "Jump to start";
    return (
      <li className="history-item" key={props.history[index].key}>
        <button onClick={() => props.onClick(index)}>{label}</button>
      </li>
    );
  });
  return <ul className="History">{moves}</ul>;
};

export default History;
